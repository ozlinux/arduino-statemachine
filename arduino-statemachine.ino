

/** Transition is the type of function that is called when a state changes */
typedef void (*Transition)();

/** Worker is the type of function that does the work in each state */
typedef void (*Worker)();

/** List of all possible inputs */
typedef enum {
  InputButton,
  InputT,
  InputH
} Input;

/** Number of different inputs */
#define  NumInput 3

/** Forward declaration of a struct */
typedef struct State State;

/** A state has a task and a list of what to do with inputs */
typedef struct State {
  Worker worker;
  State **next_states;
  Transition *actions;
} ;

/* Some transitions cause a function to be called */
void IdleToTemp();
void TempToHumid();
void HumidToTemp();
void TempToIdle();
void HumidToIdle();

/** Each state has a task running */
void IdleTask();
void TempTask();
void HumidTask();

/* Here are the states */
State IdleState;
State HumidState;
State TempState;

/* Define the relationship between input and next state */
/*                                       button        temp         humid        */
State *IdleNextState[NumInput]       = { &TempState,  &IdleState,  &IdleState };
State *TempNextState[NumInput]       = { &IdleState,  &TempState,  &HumidState };
State *HumidNextState[NumInput]      = { &IdleState,  &TempState,  &HumidState };

/* Define the actions that happen for each input */
/*                                       button        temp         humid        */
Transition IdleTransition[NumInput]  = { IdleToTemp,  NULL,        NULL };
Transition TempTransition[NumInput]  = { TempToIdle,  NULL,        TempToHumid };
Transition HumidTransition[NumInput] = { HumidToIdle, HumidToTemp, NULL };

/** Initialise the states with task and transitions */
void InitStates()
{
  IdleState = { .worker = IdleTask, .next_states = IdleNextState, .actions = IdleTransition };
  HumidState = { .worker = HumidTask, .next_states = HumidNextState, .actions = HumidTransition };
  TempState = { .worker = TempTask, .next_states = TempNextState, .actions = TempTransition };
}

/** Global state variable */
State *CurrentState = &IdleState;

/** Given a state and an input, find out what to do */
void Dispatch(Input input)
{
  Transition action = CurrentState->actions[input];
  if (action != NULL) {
    (*action)();
  }
  State *next_state = CurrentState->next_states[input];
  if (next_state != NULL) {
    CurrentState = next_state;
  }
}

void HandleInput()
{
  if (Serial.available()) {
    String str = Serial.readString();
    char ch = str.charAt(0);
    switch (ch) {
      case 'b':
        Dispatch(InputButton);
        break;
      case 't':
        Dispatch(InputT);
        break;
      case 'h':
        Dispatch(InputH);
        break;
      default:
        break;
    }
  }
}

void setup()
{
  Serial.begin(115200);
  InitStates();
}

void loop()
{
  while (true) {
    // Get input and do state change
    HandleInput();
    // Now do the work for the state
    (*CurrentState->worker)();
  }
}

void IdleToTemp()
{
  Serial.println("Was Idle now going to Temp");
}

void TempToHumid()
{
  Serial.println("Was Temp now going to Humid");
}

void HumidToTemp()
{
  Serial.println("Was Humid now going to Temp");
}
void TempToIdle()
{
  Serial.println("Was Temp now going to Idle");
}

void HumidToIdle()
{
  Serial.println("Was Humid now going to Idle");
}

void IdleTask()
{
  static long unsigned last = 0;
  if (millis() - last > 1000) {
    Serial.println("Machine is idle");
    last = millis();
  }
}

void TempTask()
{
  static long unsigned last = 0;
  if (millis() - last > 1000) {
    Serial.println("Machine is doing temperature");
    last = millis();
  }
}

void HumidTask()
{
  static long unsigned last = 0;
  if (millis() - last > 1000) {
    Serial.println("Machine is doing humidity");
    last = millis();
  }
}
